--select * from bond

--Question 1 show all info for bond CUSIP 28717RH95
--select * from bond where CUSIP = '28717RH95'

--Question 2; show all bonds from earliest to latest maturity
--select * from bond order by maturity asc 

--Question 3; Calculate the value of this bond portfolio
--select sum(price * quantity) from bond 

--Question 4; Annual return of each bond
--select CUSIP, sum(quantity * price * (coupon/100)) from bond group by CUSIP

--Question 5; Show bonds only of a certain quality and above AA2
--select CUSIP, rating from bond where rating in ('AAA', 'AA2', 'AA1')

--Question 6;Show the average price & coupon rate for bond ratings
--select rating, avg(price), avg(coupon) from bond group by rating

--Q7;Calculate the yield for each bond, as the ratio of coupon to price.
--dentify bonds that we might consider to be overpriced, as those whose 
--yield is less than the expected yield given the rating of the bond.   

select b.CUSIP, b.rating, b.coupon / b.price as yield, r.expected_yield
from bond b
  join rating r on b.rating = r.rating
  where b.coupon/b.price < r.expected_yield





